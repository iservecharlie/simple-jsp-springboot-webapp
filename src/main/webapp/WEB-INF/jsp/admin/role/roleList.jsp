<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<c:if test="${systemMessage.type != 'BLANK'}">
    <span class="${systemMessage.type}">${systemMessage.message}</span>
</c:if>
<spring:errors path="roleForm.*" cssClass="error" element="div"/>
<table>
    <thead>
    <tr>
        <th>id</th>
        <th>Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>

    <c:forEach var="role" items="${roles}">
        <tr>
            <td><c:out value="${role.id}" default="[role id not found]"/></td>
            <td><c:out value="${role.name}" default="[role name not found]"/></td>
            <td><button onclick="location.href='<c:url value="/role/${role.id}"/>';">Edit</button></td>
            <td><button onclick="deleteRole(${role.id});">Delete</button></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<button onclick="location.href='<c:url value="/role/new"/>';">New</button>
<spring:form action="" method="post" name="roleForm" id="roleForm">
    <input type="hidden" value="" name="roleId" id="roleId"/>
</spring:form>
<script type="text/javascript">
    function deleteRole(roleId) {
        document.getElementById("roleId").value = roleId;
        document.getElementById("roleForm").action = "/role/" + roleId + "/delete";
        document.getElementById("roleForm").submit();
    }
</script>