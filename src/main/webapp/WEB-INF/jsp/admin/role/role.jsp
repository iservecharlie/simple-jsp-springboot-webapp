<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${systemMessage.type != 'BLANK'}">
    <span class="${systemMessage.type}">${systemMessage.message}</span>
</c:if>
<spring:errors path="roleForm.*" cssClass="error" element="div"/>
<spring:form modelAttribute="roleForm" action="/role/${roleForm.roleId}" method="post">
    <spring:hidden path="roleId"/>
    <spring:label path="name"><spring:input path="name"/></spring:label>
    <spring:button value="submit">Submit</spring:button>
</spring:form>