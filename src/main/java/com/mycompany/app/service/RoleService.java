package com.mycompany.app.service;

import com.mycompany.app.entity.Role;
import com.mycompany.app.form.RoleForm;

import java.util.List;

public interface RoleService {
    List<Role> getAllRoles();
    Role getRole(Long roleId);
    Role getRoleByName(String roleName);

    Role saveRole(RoleForm roleForm);
    Role deleteRole(Long roleId);
}
