package com.mycompany.app.repository;

import com.mycompany.app.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long>{
    List<Role> findByName(String name);
}
