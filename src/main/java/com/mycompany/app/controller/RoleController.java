package com.mycompany.app.controller;

import com.mycompany.app.entity.Role;
import com.mycompany.app.model.SystemMessage;
import com.mycompany.app.form.RoleForm;
import com.mycompany.app.service.RoleService;
import com.mycompany.app.validator.RoleCreationValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Objects;
import java.util.Optional;

@Controller
@RequestMapping("/role")
public class RoleController {
    private final RoleService roleService;
    private final RoleCreationValidator roleCreationValidator;

    public RoleController(RoleService roleService, RoleCreationValidator roleCreationValidator) {
        this.roleService = roleService;
        this.roleCreationValidator = roleCreationValidator;
    }

    @RequestMapping(value = {"","/","/list"}, method = RequestMethod.GET)
    public String roleList(@ModelAttribute("systemMessage") SystemMessage systemMessage, ModelMap modelMap) {
        modelMap.addAttribute("roles", roleService.getAllRoles());
        if(!SystemMessage.Type.BLANK.equals(systemMessage.getType())){
            modelMap.addAttribute("systemMessage", systemMessage);
        }
        return "/admin/role/roleList";
    }

    @RequestMapping(value = {"/{id}/delete"}, method = RequestMethod.POST)
    public String deleteRole(@RequestParam("roleId") Long roleId, @PathVariable(name = "id") Long id,
                             RedirectAttributes redirectAttributes) {
        if(roleId.equals(id)){
            Role role = roleService.deleteRole(roleId);
            redirectAttributes.addFlashAttribute("systemMessage", new SystemMessage(SystemMessage.Type.SUCCESS, "Deleted role " + role.getName() + "."));
        } else {
            redirectAttributes.addFlashAttribute("systemMessage", new SystemMessage(SystemMessage.Type.ERROR, "Could not find role to delete."));
        }
        return "redirect:/role/list";
    }

    @RequestMapping(value = {"/{id}", "/new"}, method = RequestMethod.GET)
    public String editRole(@PathVariable(name="id", required = false) Optional<Long> roleId,
                           @ModelAttribute("systemMessage") SystemMessage systemMessage,
                           ModelMap modelMap) {
        if(roleId.isPresent()){
            Role role = roleService.getRole(roleId.get());
            modelMap.addAttribute("roleForm", new RoleForm(role));
        } else {
            modelMap.addAttribute("roleForm", new RoleForm());
        }

        if(!SystemMessage.Type.BLANK.equals(systemMessage.getType())){
            modelMap.addAttribute("systemMessage", systemMessage);
        }
        return "/admin/role/role";
    }

    @RequestMapping(value = {"/{id}", "/"}, method = RequestMethod.POST)
    public String saveRole(@PathVariable(name="id", required = false) Optional<Long> roleId,
                           @ModelAttribute("roleForm") @Valid RoleForm roleForm,
                           BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        roleCreationValidator.validate(roleForm, bindingResult);
        if(bindingResult.hasErrors()){
            return "/admin/role/role";
        }
        Role role = roleService.saveRole(roleForm);
        redirectAttributes.addFlashAttribute("systemMessage", new SystemMessage(SystemMessage.Type.SUCCESS, "Saved role " + role.getName() + "."));
        return "redirect:/role/" + (roleId.isPresent()? role.getId() : "list");
    }
}
